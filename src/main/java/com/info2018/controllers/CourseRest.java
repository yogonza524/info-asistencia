/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.info2018.controllers;

import com.google.gson.Gson;
import com.info2018.model.Asistencia;
import com.info2018.services.ClassRoomService;
import com.info2018.services.EmailService;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author gonzalo
 */
@RestController
public class CourseRest {
    
    @Autowired
    private ClassRoomService classRoomService;
    
    @Autowired
    private EmailService emailService;
    
    @Autowired
    private Gson prettyGson;
    
    @RequestMapping(value = "/students", method = RequestMethod.GET)
    public Map students() {
        Map result = result();
        
        try {
            result.put("data", classRoomService.getStudents());
        } catch (GeneralSecurityException ex) {
            Logger.getLogger(CourseRest.class.getName()).log(Level.SEVERE, null, ex);
            result.put("status", 500);
            result.put("exception", ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(CourseRest.class.getName()).log(Level.SEVERE, null, ex);
            result.put("status", 500);
            result.put("exception", ex.getMessage());
        }
        
        return result;
    }
    
    @RequestMapping(value = "/asistencia", method = RequestMethod.POST)
    public Map asistencias(@RequestBody Asistencia a){
        
        System.out.println("Recibido: " + prettyGson.toJson(a));
        
        Map result = result();
        
        if (a.getPresentes() != null && !a.getPresentes().isEmpty()) {
            StringBuilder message = new StringBuilder();
            
            a.getPresentes().stream().forEach(c -> {
                message.append(c).append("<br />");
            });
            
            new Thread(() -> {
                try {
                    emailService.sendSimpleMessage("yogonza524@gmail.com", "[ASISTENCIA] " 
                            + a.getFecha() , message.toString());
                } catch (MessagingException ex) {
                    Logger.getLogger(CourseRest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
            
        }
        
        result.put("message", "OK");
        result.put("status", 200);
        
        return result;
    }
    
    private Map result() {
        Map<String,Object> r = new HashMap<>();
        
        r.put("status", 200);
        r.put("timestamp", System.currentTimeMillis());
        
        return r;
    }
}
