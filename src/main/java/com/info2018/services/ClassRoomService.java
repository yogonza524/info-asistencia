/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.info2018.services;

import com.google.api.services.classroom.Classroom;
import com.google.api.services.classroom.Classroom.Courses;
import com.google.api.services.classroom.Classroom.Courses.Get;
import com.google.api.services.classroom.Classroom.Courses.Students;
import com.google.api.services.classroom.model.Course;
import com.google.api.services.classroom.model.ListCoursesResponse;
import com.google.api.services.classroom.model.ListStudentsResponse;
import com.google.api.services.classroom.model.Student;
import com.info2018.config.ClassRoomConfig;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gonzalo
 */
@Service
public class ClassRoomService {
    
    @Autowired
    private ClassRoomConfig classRoomConfig;
    
    public ListCoursesResponse listCourses(int maxCourses) throws 
            GeneralSecurityException, IOException {
        return getClassRoom().courses().list()
                .setPageSize(10)
                .execute();
    }
    
    public Optional<Course> getInfo2018Course() throws GeneralSecurityException, IOException {
        ListCoursesResponse courses = listCourses(1);
        
        Optional<Course> result = courses
                .getCourses()
                .stream()
                .filter(
                        c -> c.getName()
                              .equals(classRoomConfig.getCourses().get(0)))
                .findFirst();
        
        return result;
    }
    
    public Classroom getClassRoom() throws GeneralSecurityException, IOException {
        return classRoomConfig.getClassRoom();
    }
    
    public List<Student> getStudents() throws GeneralSecurityException, IOException {
        Classroom info2018 = getClassRoom();
        Optional<Course> curso = getInfo2018Course();
        if (curso.isPresent()) {
            return info2018.courses()
                    .students()
                    .list(curso.get().getId()).execute().getStudents();
        }
        return null;
    }
}
