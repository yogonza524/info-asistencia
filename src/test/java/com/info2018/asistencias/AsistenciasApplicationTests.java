package com.info2018.asistencias;

import com.google.api.services.classroom.model.Course;
import com.google.api.services.classroom.model.ListCoursesResponse;
import com.google.gson.Gson;
import com.info2018.config.ClassRoomConfig;
import com.info2018.services.ClassRoomService;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AsistenciasApplicationTests {
    
    @Autowired
    private ClassRoomConfig config;
    
    @Autowired
    private ClassRoomService classRoomService;
    
    @Autowired
    private Gson prettyGson;
    
    @Test
    public void contextLoads() {
        
    }

    @Test
    public void showConfig() {
        System.out.println("App name: " + config.getAppName());
        System.out.println("Credentials: " + config.getCredentialsPath());
        System.out.println("Tokens Directory: " + config.getTokensDirectoryPath());
    }
    
    @Test
    @Ignore
    public void listAllCoursesTest() throws IOException, GeneralSecurityException {
        // List the first 10 courses that the user has access to.
        ListCoursesResponse response = config.getClassRoom().courses().list()
                .setPageSize(10)
                .execute();
        List<Course> courses = response.getCourses();
        if (courses == null || courses.size() == 0) {
            System.out.println("No courses found.");
        } else {
            System.out.println("Courses:");
            for (Course course : courses) {
                System.out.printf("%s\n", course.getName());
                System.out.printf("%s\n", course.getId());
            }
        }
    }
    
    @Test
    @Ignore
    public void classRoomTest() throws GeneralSecurityException, IOException {
        classRoomService.getInfo2018Course().ifPresent(c -> {
            System.out.println("Name of course: " + c.getName());
            System.out.println("Course Group Email: " + c.getCourseGroupEmail());
            
        });
    }
    
    @Test
    @Ignore
    public void listStudentsTest() throws GeneralSecurityException, IOException {
        System.out.println("Students");
//        classRoomService.getStudents().list("15952356885").executeUsingHead();
        classRoomService.getStudents().stream().forEach(c -> {
            System.out.println(prettyGson.toJson(c));
        });
    }
}
